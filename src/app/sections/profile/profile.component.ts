import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { empty } from 'rxjs';

@Component({
  selector: 'app-profile:id',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  constructor(private httpClient:HttpClient,private route:ActivatedRoute) { }

  private id:any;
  private isEmpty:any=[];
  private profileData:any=[
    {
        "id": 8,
        "name": "Pete",
        "surname": "Ninon",
        "adress": "St. Andy's Walk 811",
        "country": "Suriname",
        "phone": [],
        "email": [],
        "tag": []
    }
];


  ngOnInit() {
    this.route.params.subscribe(params=>{this.id=params});
    this.httpClient.get("https://localhost:5001/api/contact/"+this.id.id)
    .subscribe(
      (data)=>{
        this.profileData=data;
        console.log(this.profileData);
      }
    );    
  }

}
