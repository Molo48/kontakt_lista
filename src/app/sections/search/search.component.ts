import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  searched:string='';
  gotContactData:any;
  searchOption:string;
  inputString:string='';
  gotTagData:any;
  constructor(private httpClient:HttpClient) { }

  ngOnInit() { }

  startSearch(){
    this.gotContactData=[];
    this.gotTagData=[];
    this.searched=this.inputString;
    
    console.log(this.searched);

    if(this.searchOption=="name")
    this.httpClient.get("https://localhost:5001/api/contact/name="+this.searched)
    .subscribe(
      (data)=>{
        this.gotContactData=data;
        console.log(data);
      }
    );

    if(this.searchOption=="surname")
    this.httpClient.get("https://localhost:5001/api/contact/surname="+this.searched)
    .subscribe(
      (data)=>{
        this.gotContactData=data;
        console.log(data);
      }
    );
/*
    if(this.searchOption=="tag")
    this.httpClient.get("https://localhost:5001/api/tag/tagName="+this.searched)
    .subscribe(
      (data)=>{
        this.gotTagData=data;
        console.log(data);
      }
    ); */
  }
}
