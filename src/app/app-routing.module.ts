import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SearchComponent } from './sections/search/search.component';
import { NewContactComponent } from './sections/new-contact/new-contact.component';
import { ProfileComponent } from './sections/profile/profile.component';

const routes: Routes = [
  {path:'search',component:SearchComponent},
  {path:'newContact',component:NewContactComponent},
  {path:'profile/:id',component:ProfileComponent},


  {path:'',component:SearchComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
