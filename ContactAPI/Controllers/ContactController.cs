using System.Linq;
using ContactAPI.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace ContactAPI.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("SiteCorsPolicy")]
    public class ContactController:Controller
    {
        private readonly ApiContext _ctx;

        public ContactController(ApiContext ctx)
        {
            _ctx=ctx; 
        }

        //GET api/contact
        [HttpGet]
        public IActionResult Get()
        {
            var data=_ctx.Contacts.OrderBy(c=>c.Id);

            return Ok(data);
        }

        //GET api/contact/5
        [HttpGet("{id}",Name="GetContact")]
        public IActionResult GetContact(int id)
        {
            var data=(from c in _ctx.Contacts
            where c.Id==id
            select new {
                Id=c.Id,
                Name=c.Name,
                Surname=c.Surname,
                Adress=c.Adress,
                Country=c.Country,
                Phone=(from p in _ctx.Phones
                    where c.Id==p.Contact.Id
                    select new {                        
                        Phone=p.PhoneNum, 
                    }
                    ).ToArray(),
                Email=(from e in _ctx.Emails
                    where c.Id==e.Contact.Id
                    select new {                        
                        Email=e.EmailAdress, 
                    }
                    ).ToArray(),
                Tag=(from t in _ctx.Tags
                    where c.Id==t.Contact.Id
                    select new {                        
                        Tag=t.TagName, 
                    }
                    ).ToArray(),
            }
            );
            return Ok(data);
        }

        /* //GET api/contact/name=denis
        [HttpGet("name={name}",Name="GetContactByName")]
        public IActionResult GetContactByName(string name)
        {
            var data=_ctx.Contacts.Where(c=>c.Name.ToLower()==name.ToLower());

            return Ok(data);
        } */
        


        
        //GET api/contact/name=denis
        [HttpGet("name={name}",Name="GetContactByName")]
        public IActionResult GetContactByName(string name)
        {
            var data=(from c in _ctx.Contacts
            where c.Name.ToLower().Contains(name.ToLower())
            select new {
                Id=c.Id,
                Name=c.Name,
                Surname=c.Surname,
                Phone=(from p in _ctx.Phones
                    where c.Id==p.Contact.Id
                    select new {                        
                        Phone=p.PhoneNum, 
                    }
                    ).ToArray(),
                Email=(from e in _ctx.Emails
                    where c.Id==e.Contact.Id
                    select new {                        
                        Email=e.EmailAdress, 
                    }
                    ).ToArray(),
                Tag=(from t in _ctx.Tags
                    where c.Id==t.Contact.Id
                    select new {                        
                        Tag=t.TagName, 
                    }
                    ).ToArray(),
            }
            );
            return Ok(data);
        }
        
        /* //GET api/contact/surname=gibbs
        [HttpGet("surname={surname}",Name="GetContactBySurname")]
        public IActionResult GetContactBySurname(string surname)
        {
            var data=_ctx.Contacts.Where(c=>c.Surname.ToLower()==surname.ToLower());

            return Ok(data);
        } */

        //GET api/contact/surname=gibbs
        [HttpGet("surname={surname}",Name="GetContactBySurname")]
        public IActionResult GetContactBySurname(string surname)
        {
            var data=(from c in _ctx.Contacts
            where c.Surname.ToLower().Contains(surname.ToLower())
            select new {
                Id=c.Id,
                Name=c.Name,
                Surname=c.Surname,
                Phone=(from p in _ctx.Phones
                    where c.Id==p.Contact.Id
                    select new {                        
                        Phone=p.PhoneNum, 
                    }
                    ).ToArray(),
                Email=(from e in _ctx.Emails
                    where c.Id==e.Contact.Id
                    select new {                        
                        Email=e.EmailAdress, 
                    }
                    ).ToArray(),
                Tag=(from t in _ctx.Tags
                    where c.Id==t.Contact.Id
                    select new {                        
                        Tag=t.TagName, 
                    }
                    ).ToArray(),
            }
            );
            return Ok(data);
        }

        //POST api/contact
        [HttpPost]
        public IActionResult Post([FromBody] Contact contact)
        {
            if(contact==null)
            {
                return BadRequest();
            }

            _ctx.Contacts.Add(contact);
            
            _ctx.SaveChanges();
            
            return CreatedAtRoute("GetContact",new {id=contact.Id},contact);
        }

        //DELETE api/contact/id=4  //deletes contact from all tables
        [HttpDelete("id={id}", Name="DeleteContactById")]
        public IActionResult DeleteContactById(int id)
        {
            var contact=_ctx.Contacts.Find(id);
            var phones=_ctx.Phones.Where(o=>o.Contact.Id==id);
            var emails=_ctx.Emails.Where(o=>o.Contact.Id==id);
            var tags=_ctx.Tags.Where(o=>o.Contact.Id==id);

            foreach (var phone in phones)
            {
                _ctx.Phones.Remove(phone);  
            }
            
            foreach (var email in emails)
            {
                _ctx.Emails.Remove(email);  
            }

            foreach (var tag in tags)
            {
                _ctx.Tags.Remove(tag);  
            }

            var data=_ctx.Contacts.Remove(contact);
            _ctx.SaveChanges();
            return Ok(data);
        }
    }
}