using System.Linq;
using ContactAPI.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace ContactAPI.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("SiteCorsPolicy")]
    public class EmailController:Controller
    {
        private readonly ApiContext _ctx;

        public EmailController(ApiContext ctx)
        {
            _ctx=ctx;
        }

        //GET api/email
        [HttpGet]
        public IActionResult Get()
        {
            var data=_ctx.Emails.OrderBy(c=>c.Id);

            return Ok(data);
        }

        //GET api/email/id=5
        [HttpGet("id={id}",Name="GetEmail")]
        public IActionResult GetEmail(int id)
        {
            var email=_ctx.Emails.Find(id);

            return Ok(email);
        }

        //GET api/email/contactId=5
        [HttpGet("contactId={id}",Name="GetEmailByContactId")]
        public IActionResult GetEmailByContactId(int id)
        {
            var data=_ctx.Emails.Where(c=>c.Contact.Id==id);

            return Ok(data);
        }

        //GET api/email/emailAdress=x.x@xmail.xom
        [HttpGet("emailAdress={emailAdress}",Name="GetEmailByAdress")]
        public IActionResult GetEmailByAdress(string emailAdress)
        {
            var data=_ctx.Emails.Where(c=>c.EmailAdress==emailAdress);
            return Ok(data);
        }

        //POST api/email
        [HttpPost]
        public IActionResult Post([FromBody] Email email)
        {
            if(email==null)
            {
                return BadRequest();
            }

            _ctx.Emails.Add(email);
            _ctx.SaveChanges();
            
            return CreatedAtRoute("GetEmail",new {id=email.Id},email);
        }

        //DELETE api/email/id=4   //deletes email whitch id is 4
         [HttpDelete("id={id}", Name="DeleteEmailById")]
         public IActionResult DeleteEmailById(int id)
         {
             var email=_ctx.Emails.Find(id);

             _ctx.Emails.Remove(email);
             _ctx.SaveChanges();
             return Ok(email);
         }

         //DELETE api/email/contactid=4   //deletes email whitch contactId is 4
         [HttpDelete("contactId={id}", Name="DeleteEmailByContactId")]
         public IActionResult DeleteEmailByContactId(int id)
         {
             var emails=_ctx.Emails.Where(c=>c.Contact.Id==id);
            
            foreach (var email in emails)
            {
             _ctx.Emails.Remove(email);
            }
             _ctx.SaveChanges();

             return Ok(emails);
         }
    }
}