using System.Linq;
using ContactAPI.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace ContactAPI.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("SiteCorsPolicy")]
    public class PhoneController:Controller
    {
        private readonly ApiContext _ctx;

        public PhoneController(ApiContext ctx)
        {
            _ctx=ctx;
        }

        //GET api/phone
        [HttpGet]
        public IActionResult Get()
        {
            var data=_ctx.Phones.OrderBy(c=>c.Id);

            return Ok(data);
        }

        //GET api/phone/id=5
        [HttpGet("id={id}",Name="GetPhone")]
        public IActionResult GetPhone(int id)
        {
            var phone=_ctx.Phones.Find(id);

            return Ok(phone);
        }

        //GET api/phone/contactId=5
        [HttpGet("contactId={id}",Name="GetPhoneByContactId")]
        public IActionResult GetPhoneByContactId(int id)
        {
            var data=_ctx.Phones.Where(c=>c.Contact.Id==id);

            return Ok(data);
        }

        //GET api/phone/phoneNum=+21344
        [HttpGet("phoneNum={num}",Name="GetPhoneByNumber")]
        public IActionResult GetPhoneByNumber(string num)
        {
            var data=_ctx.Phones.Where(c=>c.PhoneNum==num);
            return Ok(data);
        }

        //POST api/phone
        [HttpPost]
        public IActionResult Post([FromBody] Phone phone)
        {
            if(phone==null)
            {
                return BadRequest();
            }

            _ctx.Phones.Add(phone);
            _ctx.SaveChanges();
            
            return CreatedAtRoute("GetPhone",new {id=phone.Id},phone);
        }

        //DELETE api/phone/id=4   //deletes phone whitch id is 4
         [HttpDelete("id={id}", Name="DeletePhoneById")]
         public IActionResult DeletePhoneById(int id)
         {
             var phone=_ctx.Phones.Find(id);

             _ctx.Phones.Remove(phone);
             _ctx.SaveChanges();
             return Ok(phone);
         }

         //DELETE api/phone/contactId=4   //deletes phone whitch contactId is 4
         [HttpDelete("contactId={id}", Name="DeletePhoneByContactId")]
         public IActionResult DeletePhoneByContactId(int id)
         {
             var phones=_ctx.Phones.Where(c=>c.Contact.Id==id);

            foreach (var phone in phones)
            {
             _ctx.Phones.Remove(phone);
            }
             _ctx.SaveChanges();

             return Ok(phones);
         }
    }
}