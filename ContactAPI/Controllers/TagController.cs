using System.Linq;
using ContactAPI.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace ContactAPI.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("SiteCorsPolicy")]
    public class TagController:Controller
    {
        private readonly ApiContext _ctx;

        public TagController(ApiContext ctx)
        {
            _ctx=ctx;
        }

        //GET api/tag
        [HttpGet]
        public IActionResult Get()
        {
            var data=_ctx.Tags.OrderBy(c=>c.Id);

            return Ok(data);
        }

        //GET api/tag/id=5
        [HttpGet("id={id}",Name="GetTag")]
        public IActionResult GetTag(int id)
        {
            var tag=_ctx.Tags.Find(id);

            return Ok(tag);
        }

        //GET api/tag/contactId=5
        [HttpGet("contactId={id}",Name="GetTagByContactId")]
        public IActionResult GetTagByContactId(int id)
        {
            var data=_ctx.Tags.Where(c=>c.Contact.Id==id);

            return Ok(data);
        }

        /* //GET api/tag/tagName=coworker
        [HttpGet("tagName={tagName}",Name="GetTagByName")]
        public IActionResult GetTagByName(string tagName)
        {
            var data=_ctx.Tags.Where(c=>c.TagName==tagName);
            return Ok(data);
        } */


        //GET api/tag/tagName=coworker
        [HttpGet("tagName={tagName}",Name="GetTagByName")]
        public IActionResult GetTagByName(string tagName)
        {
            var data=(from t in _ctx.Tags
            where t.TagName.ToLower().Contains(tagName.ToLower())
            select new {
                Id=t.Contact.Id,
                Name=t.Contact.Name,
                Surname=t.Contact.Surname,
                Phone=(from p in _ctx.Phones
                    where t.Contact.Id==p.Contact.Id
                    select new {                        
                        Phone=p.PhoneNum, 
                    }
                    ).ToArray(),
                Email=(from e in _ctx.Emails
                    where t.Contact.Id==e.Contact.Id
                    select new {                        
                        Email=e.EmailAdress, 
                    }
                    ).ToArray(),
                Tag=(from t2 in _ctx.Tags
                    where t.Contact.Id==t2.Contact.Id
                    select new {                        
                        Tag=t.TagName, 
                    }
                    ).ToArray(),
            }
            );
            return Ok(data);
        }

        //POST api/tag
        [HttpPost]
        public IActionResult Post([FromBody] Tag tag)
        {
            if(tag==null)
            {
                return BadRequest();
            }

            _ctx.Tags.Add(tag);
            _ctx.SaveChanges();
            
            return CreatedAtRoute("GetTag",new {id=tag.Id},tag);
        }

        //DELETE api/tag/id=4   //deletes tag whitch id is 4
         [HttpDelete("id={id}", Name="DeleteTagById")]
         public IActionResult DeleteTagById(int id)
         {
             var tag=_ctx.Tags.Find(id);

             _ctx.Tags.Remove(tag);
             _ctx.SaveChanges();
             return Ok(tag);
         }

         //DELETE api/tag/contactid=4   //deletes tag whitch contactId is 4
         [HttpDelete("contactId={id}", Name="DeleteTagByContactId")]
         public IActionResult DeleteTagByContactId(int id)
         {
             var tags=_ctx.Tags.Where(c=>c.Contact.Id==id);
            
            foreach (var tag in tags)
            {
             _ctx.Tags.Remove(tag);
            }
             _ctx.SaveChanges();

             return Ok(tags);
         }
    }
}