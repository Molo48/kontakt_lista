namespace ContactAPI.Models
{
    public class Phone
    {
        public int Id {get; set;}
        public string PhoneNum {get; set;}
        public Contact Contact {get; set;}
    }
}