namespace ContactAPI.Models
{
    public class Email
    {
        public int Id {get; set;}
        public string EmailAdress {get; set;}
        public Contact Contact {get; set;}
    }
}