using Microsoft.EntityFrameworkCore;

namespace ContactAPI.Models
{
    public class ApiContext: DbContext
    {
        public ApiContext(DbContextOptions<ApiContext>options):base(options){}

        public DbSet<Contact> Contacts {get; set;}
        public DbSet<Email> Emails {get; set;}
        public DbSet<Phone> Phones {get; set;}
        public DbSet<Tag> Tags {get; set;}

    }
}