using System;
using System.Collections.Generic;
using ContactAPI.Models;

namespace ContactAPI
{
    public class Helpers
    {
        private static Random _rand=new Random();
        private static string GetRandom(IList<string> items)
        {
            return items[_rand.Next(items.Count)];
        }


        internal static string MakeContactName()
        {
            var name=GetRandom(nameList);
            return name;
        }
        private static readonly List<string> nameList =new List<string>()
        {
            "Adam", "Andy", "Antony", "Ana", "Anabell",
            "Brand","Bruce", "Bob","Bethany",
            "Chris", "Christina",
            "Denis", "Eve", "Marco","Michael","Nathalie", "Pete", "Paula"
        };
        internal static string MakeContactSurname()
        {
            var surname=GetRandom(surnameList);
            return surname;
        }

        private static readonly List<string> surnameList =new List<string>()
        {
            "Aron","Flynn","Gibbs", "Mills",
            "Norris","Owens","White","Smith",
            "Holmes", "Lee", "Shang", "Yung",
            "Ninon" 
        };

        internal static string MakeContactAdress()
        {
            var adressSuffix=GetRandom(adressSuffixList);
            var adressNamePrefix=GetRandom(nameList);
            var adressNumber=_rand.Next(5000);
            return "St. "+adressNamePrefix+"'s "+adressSuffix+" "+adressNumber;
        }

        private static readonly List<string> adressSuffixList =new List<string>()
        {
            "Street","Road","Alley","Avenue","Yard","Walk","Hill"
        };

        internal static string MakeContactCountry()
        {
            var country=GetRandom(countryList);
            
            return country;
        }

        private static readonly List<string> countryList =new List<string>()
        {
            "Afghanistan","Albania","Algeria","Andorra","Angola","Antigua & Deps","Argentina","Armenia","Australia","Austria","Azerbaijan",
            "Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bhutan","Bolivia","Bosnia Herzegovina","Botswana",
            "Brazil","Brunei","Bulgaria","Burkina","Burundi","Cambodia","Cameroon","Canada","Cape Verde","Central African Rep",
            "Chad","Chile","China","Colombia","Comoros","Congo","Congo {Democratic Re","Costa Rica","Croatia","Cuba","Cyprus",
            "Czech Republic","Denmark","Djibouti","Dominica","Dominican Republic",
            "East Timor","Ecuador","Egypt","El Salvador","Equatorial Guinea","Eritrea","Estonia","Ethiopia","Fiji","Finland","France",
            "Gabon","Gambia","Georgia","Germany","Ghana","Greece","Grenada","Guatemala","Guinea","Guinea-Bissau","Guyana","Haiti","Honduras","Hungary",
            "Iceland","India","Indonesia","Iran","Iraq","Ireland {Republic}","Israel","Italy","Ivory Coast",
            "Jamaica","Japan","Jordan","Kazakhstan","Kenya","Kiribati","Korea North","Korea South","Kosovo","Kuwait","Kyrgyzstan",
            "Laos","Latvia","Lebanon","Lesotho","Liberia","Libya","Liechtenstein","Lithuania","Luxembourg",
            "Macedonia","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Marshall Islands","Mauritania","Mauritius","Mexico",
            "Micronesia","Moldova","Monaco","Mongolia","Montenegro","Morocco","Mozambique","Myanmar, {Burma}","Namibia","Nauru","Nepal",
            "Netherlands","New Zealand","Nicaragua","Niger","Nigeria","Norway","Oman","Pakistan","Palau","Panama","Papua New Guinea",
            "Paraguay","Peru","Philippines","Poland","Portugal","Qatar","Romania","Russian Federation","Rwanda","St Kitts & Nevis","St Lucia",
            "Saint Vincent & the Grenadines","Samoa","San Marino","Sao Tome & Principe","Saudi Arabia","Senegal","Serbia","Seychelles","Sierra Leone",
            "Singapore","Slovakia","Slovenia","Solomon Islands","Somalia","South Africa","South Sudan","Spain","Sri Lanka","Sudan","Suriname","Swaziland",
            "Sweden","Switzerland","Syria","Taiwan","Tajikistan","Tanzania","Thailand","Togo","Tonga","Trinidad & Tobago","Tunisia","Turkey","Turkmenistan",
            "Tuvalu","Uganda","Ukraine","United Arab Emirates","United Kingdom","United States","Uruguay","Uzbekistan","Vanuatu","Vatican City","Venezuela",
            "Vietnam","Yemen","Zimbabwe","Zambia",
        };
        internal static string MakePhoneNum()
        {
            var prefixNum=_rand.Next(1000);
            var num=_rand.Next(1000000000);
            return "+"+prefixNum+" "+num;
        }
        internal static string MakeEmailMail(Contact contact)
        {
            var num=_rand.Next(100);
            var emailProvider=GetRandom(emailProviderList);
            return contact.Name.ToLower()+"."+contact.Surname.ToLower()+"."+num+"@"+emailProvider;
        }

        private static readonly List<string> emailProviderList =new List<string>()
        {
            "gmail.com","yahoo.com","hotmail.com","outlook.com","icloud.com","fesb.hr","net.hr",
        };

        internal static string MakeTagName()
        {
            var tagProvider=GetRandom(tagProviderList);
            return tagProvider;
        }

        private static readonly List<string> tagProviderList =new List<string>()
        {
            "friend", "coworker","acquaintance"
        };
    }
}