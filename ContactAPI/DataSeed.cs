using System;
using System.Collections.Generic;
using System.Linq;
using ContactAPI.Models;

namespace ContactAPI
{
    public class DataSeed
    {
        private readonly ApiContext _ctx;
        private static Contact _contact;
        private static Random _rand=new Random();
        public DataSeed(ApiContext ctx)
        {
            _ctx=ctx;
        }

        public void SeedData(int nContacts, int nPhones, int nEmails, int nTags)
        {
            if(!_ctx.Contacts.Any())
            {
                SeedContacts(nContacts);
                _ctx.SaveChanges();
            }

            if(!_ctx.Phones.Any())
            {
                SeedPhones(nPhones);
                _ctx.SaveChanges();
            }

            if(!_ctx.Emails.Any())
            {
                SeedEmails(nEmails);
                _ctx.SaveChanges();
            }

            if(!_ctx.Tags.Any())
            {
                SeedTags(nTags);
                _ctx.SaveChanges();
            }
        }

        private void SeedContacts(int n)
        {
            List<Contact> contacts=BuildContactList(n);

            foreach (var contact in contacts)
            {
                _ctx.Contacts.Add(contact);
            }
        }

        private void SeedPhones(int n)
        {
            List<Phone> phones=BuildPhoneList(n);

            foreach (var phone in phones)
            {
                _ctx.Phones.Add(phone);
            }
        }

        private void SeedEmails(int n)
        {
            List<Email> emails=BuildEmailList(n);

            foreach (var email in emails)
            {
                _ctx.Emails.Add(email);
            }
        }

        private void SeedTags(int n)
        {
            List<Tag> tags=BuildTagList(n);

            foreach (var tag in tags)
            {
                _ctx.Tags.Add(tag);
            }
        }

        private List<Contact> BuildContactList(int nContacts)
        {

            var contacts= new List<Contact>();

            for(var i=1;i<=nContacts;i++)
            {
                var name=Helpers.MakeContactName();
                var surname=Helpers.MakeContactSurname();
                var adress=Helpers.MakeContactAdress();
                var country=Helpers.MakeContactCountry();
                contacts.Add(new Contact{
                    Id=i,
                    Name=name,
                    Surname=surname,
                    Adress=adress,
                    Country=country
                });
            }
            return contacts;
        }

        private List <Phone> BuildPhoneList(int nPhones)
        {
            var phones= new List<Phone>();
            for(var i=1;i<=nPhones;i++)
            {
                var randContactId=_rand.Next(1,_ctx.Contacts.Count());
                var phoneNum=Helpers.MakePhoneNum();
                _contact=_ctx.Contacts.First(c => c.Id == randContactId);
                phones.Add(new Phone{
                    Id=i,
                    Contact=_contact,
                    PhoneNum=phoneNum
                });              
            }
            return phones;
        }

        private List <Email> BuildEmailList(int nEmails)
        {
            var emails= new List<Email>();

            for(var i=1;i<=nEmails;i++)
            {
                var randContactId=_rand.Next(1,_ctx.Contacts.Count());
                _contact=_ctx.Contacts.First(c => c.Id == randContactId);
                var emailAdress=Helpers.MakeEmailMail(_contact);
                
                emails.Add(new Email{
                    Id=i,
                    Contact=_contact,
                    EmailAdress=emailAdress
                });
            }
            return emails;
        }


        private List <Tag> BuildTagList(int nTags)
        {
            var tags= new List<Tag>();

            for(var i=1;i<=nTags;i++)
            {
                var randContactId=_rand.Next(1,_ctx.Contacts.Count());
                var tagName=Helpers.MakeTagName();
                _contact=_ctx.Contacts.First(c => c.Id == randContactId);
                
                tags.Add(new Tag{
                    Id=i,
                    Contact=_contact,
                    TagName=tagName
                });
            }
            return tags;
        }
    }
}